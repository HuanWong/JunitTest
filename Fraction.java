package Calcu;

public class Fraction {
	
	public static int[] createFraction(){
		int[] num=new int[2];
		int num1 = (int)(Math.random()*100);
		int num2 = (int)(Math.random()*100);
		if(num1!=num2){
			if(num1<num2) {
				num[0]=num1; num[1]=num2;  return num;
			}else{
				num[0]=num2; num[1]=num1;  return num;
			}
		}else  
			createFraction();               
		return num;
		
	}
	
	public static int GCD(int m, int n) {//辗转相除法求最大公约数
		if(m==0||n==0){
			return 1;
		}
		while (true) {
		if ((m = m % n) == 0)
		return n;
		if ((n = n % m) == 0)
		return m;
		}
		}
	
	public static int LCM(int m, int n){
		if(GCD(m,n)==-1){
			return -1;
		}
		return m*n/GCD(m,n);                   
	}

}

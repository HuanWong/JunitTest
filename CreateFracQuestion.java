package Calcu;

public class CreateFracQuestion {
	
	static String[] add(int[] fa1,int[] fa2,int gbs){
		String[] array = new String[2];
		array[0]="("+fa1[0]+"/"+fa1[1]+")+("+fa2[0]+"/"+fa2[1]+")= "+"\t\t";
		int sum=gbs/fa1[1]*fa1[0] + (gbs/fa2[1]*fa2[0]);
		int gys1=Fraction.GCD(sum,gbs);
		array[1]= sum/gys1+"/"+gbs/gys1+"";
		return array;	
	}
	
	static String[] sub(int[] fa1,int[] fa2,int gbs){
		String[] array = new String[2];
		if( fa1[0]/fa1[1] > fa2[0]/fa2[1] ){                       
			array[0]="("+fa1[0]+"/"+fa1[1]+")-("+fa2[0]+"/"+fa2[1]+")= "+"\t\t";
		int cha=gbs/fa1[1]*fa1[0] - (gbs/fa2[1]*fa2[0]);
		int gys2=Fraction.GCD(cha,gbs);
		array[1]= cha/gys2+"/"+gbs/gys2+"";
		return array;
		} else{
			array[0]="("+fa2[0]+"/"+fa2[1]+")-("+fa1[0]+"/"+fa1[1]+")= "+"\t\t";
			int cha=gbs/fa2[1]*fa2[0] - (gbs/fa1[1]*fa1[0]);
			int gys2=Fraction.GCD(cha,gbs);
			array[1]= cha/gys2+"/"+gbs/gys2+"";
			return array;
		}
		
	}
	
	static String[] mul(int[] fa1,int[] fa2,int gbs){
		String[] array = new String[2];
		array[0]="("+fa1[0]+"/"+fa1[1]+")×("+fa2[0]+"/"+fa2[1]+")= "+"\t\t";
		int a =fa1[0]*fa2[0]; int b = fa1[1]*fa2[1];
		int gys3 = Fraction.GCD(a,b);
		array[1]=a/gys3+"/"+b/gys3+"";
		return array;
	}
	
	static String[] div(int[] fa1,int[] fa2,int gbs){
		String[] array = new String[2];
		if(fa2[0]==0){
			CreateFQuestion();
		}else{
			array[0]="("+fa1[0]+"/"+fa1[1]+")÷("+fa2[0]+"/"+fa2[1]+")= "+"\t\t";
		int c =fa1[0]*fa2[1]; int d = fa2[0]*fa1[1];
		int gys4 = Fraction.GCD(c,d);
		array[1]= c/gys4+"/"+d/gys4+"";
		return array;
		}
		return array;
	}

	public static String[] CreateFQuestion(){
		String[] array = new String[2];
	   int[] fa1=Fraction.createFraction();
	   int[] fa2=Fraction.createFraction();
	   int gbs=Fraction.LCM(fa1[1],fa2[1]);
		int op = (int)(Math.random()*4+1);
		if(op==1){ 
			array = add(fa1,fa2,gbs);
			return array;
		}
		if(op==2){ 
			array = sub(fa1,fa2,gbs);
			
			return array;
		}
		if(op==3){ 
			array = mul(fa1,fa2,gbs);
			return array;
		}
		if(op==4){ 
			array = div(fa1,fa2,gbs);
			return array;
		}
		return array;
	}
}

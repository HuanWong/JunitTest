package Calcu;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FractionTest {
	
	static int[] num = new int[2];

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateFraction() {
		int count=1;
		for(int i=0;i<20;i++,count++){
			num=Fraction.createFraction();
		    System.out.print(num[0]+"/"+num[1]+"     ");
		    if(count>=4){System.out.println(); count =0;}
		}
		
	}

	@Test
	public void testGCD() {
		int x;
		x = Fraction.GCD(9, 6);System.out.println(x);
		x = Fraction.GCD(1, 5);System.out.println(x);
		x = Fraction.GCD(28, 6);System.out.println(x);
		x = Fraction.GCD(49, 21);System.out.println(x);
		x = Fraction.GCD(0, 0);System.out.println(x);
		x = Fraction.GCD(1, 0);System.out.println(x);
		x = Fraction.GCD(0, 1);System.out.println(x);
		System.out.println("-----分割线-----");
	}

	@Test
	public void testLCM() {
		int x;
		x = Fraction.LCM(9, 6);System.out.println(x);
		x = Fraction.LCM(1, 5);System.out.println(x);
		x = Fraction.LCM(28, 6);System.out.println(x);
		x = Fraction.LCM(49, 21);System.out.println(x);
		x = Fraction.LCM(0, 0);System.out.println(x);
		x = Fraction.LCM(1, 0);System.out.println(x);
		x = Fraction.LCM(0, 1);System.out.println(x);
		System.out.println("-----分割线-----");
	}

}
